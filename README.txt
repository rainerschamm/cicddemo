SIMPLE:
Create the new app with build config only:

oc new-app redhat-openjdk18-openshift~https://bitbucket.org/rainerschamm/cicddemo.git --name=advisor-web-quote1

Infos:
https://access.redhat.com/containers/#/registry.access.redhat.com/redhat-openjdk-18/openjdk18-openshift
https://github.com/jboss-openshift/application-templates/blob/master/openjdk/openjdk18-image-stream.json
https://access.redhat.com/documentation/en-us/openshift_online/3/html-single/using_images/index#source-to-image-s2i


Redo image streams:
oc delete is --all -n openshift
oc create -f https://raw.githubusercontent.com/jboss-openshift/application-templates/master/openjdk/openjdk18-image-stream.json
oc create -f https://raw.githubusercontent.com/openshift/origin/master/examples/image-streams/image-streams-rhel7.json


COMPLICATED:
Using a jenkins build and a binary input build config:

1) In the dev project create a new build config:
oc new-build --name=advisor-web-quote2 --image-stream=redhat-openjdk18-openshift --binary=true -e JAVA_ARGS="-Dlog.file.path=/tmp --spring.config.location=/tmp/src/application.properties" --labels=app=advisor-web-quote2 -n dev || true

2) In the cicd project that has Jenkins create a jenkins build config:
oc create -f Jenkinsfile-bc.yaml -n cicd

3) Trigger the new jenkins build. Wait for *both* builds to finish

4) Create a new app from the resulting image stream:
oc new-app advisor-web-quote2 --name=advisor-web-quote2 -n dev
